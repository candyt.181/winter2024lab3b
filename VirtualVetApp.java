import java.util.Scanner;
public class VirtualVetApp{
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		Panda[] pandas	= new Panda [4];
		
		for(int i=0; i<pandas.length; i++){
			pandas[i] = new Panda();
			System.out.println("Enter a name of the panda");
			pandas[i].name = reader.nextLine();
			System.out.println("Enter one of the two species of the panda");
			pandas[i].species = reader.nextLine();
			System.out.println("Enter the average life age of the panda in numbers");
			pandas[i].averageLifeAge = Integer.parseInt(reader.nextLine());
			
		}
		System.out.println(pandas[pandas.length-1].name);
		System.out.println(pandas[pandas.length-1].species);
		System.out.println(pandas[pandas.length-1].averageLifeAge);
		
		pandas[0].climb();
		pandas[0].eat();
		
	}
}
